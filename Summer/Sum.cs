﻿namespace Summer
{
    /* SUGERENCIAS
    * -----------
    * - Empieza del caso más simple al más complejo.
    * - Haz un commit de cada paso, para que se puedan ver los pasos seguidos durante el desarrollo.
    * - Haz commit tanto cuando crees algo como cuando refactorices.
    * - Utiliza una aproximación TDD para desarrollar la kata.
    * - Tómate tu tiempo para hacerlo, reserva un rato tranquilo.
    * - El tiempo estimado para realizarlo es entre 1 y 4 horas.
    * - El tiempo no es un factor clave, pero será tenido en cuenta.
    * - Puedes implementar cuantas clases e interfaces creas necesarios. La clase principal del juego es la presente.
    * - Implementa un contrato que sea: int Add(string valores)
    * - No es necesario probar casos de error.
    */

    /* REGLAS
     * ------
     * Se trata de una calculadora que solo suma valores.
     * 1.- Si no hay valores, recibimos una cadena vacia, devolvemos 0.
     * 2.- Dado 1 solo valor, obtendremos el mismo valor.
     * 3.- Dados 2 valores, separados por coma (','), los suma. Ejemplo: 1,2 --> Resultado: 3
     * 4.- Ampliar para sumar N valores.
     * 5.- El separador puede ser tanto una coma (',') como un Carrier Return ('\n'). Ejemplo: 1,2\n3 --> Resultado: 6
     * 6.- Podemos añadirle un separador nuevo en la misma linea, usando el formato: //nuevo_separador\nValor1,Valor2... Ejemplo: //.\n1,2\n3.4 --> Resultado: 10.
     */
    public class Sum
    {
    }
}
